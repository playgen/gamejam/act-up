﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenarioPlayer : MonoBehaviour
{
    public List<Scenario> Scenarios;

    public void Start()
    {
        StartCoroutine(Coroutine());
    }

    public IEnumerator Coroutine()
    {
        foreach (var scenario in Scenarios)
        {
            scenario.IsComplete = false;
            scenario.gameObject.SetActive(true);

            while (!scenario.IsComplete)
            {
                yield return null;
            }

            scenario.gameObject.SetActive(false);
        }

        SceneManager.LoadScene("Menu");
    }
}

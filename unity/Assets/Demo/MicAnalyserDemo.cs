﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using PlayGen.Assets.SEWA.Analyser;
using PlayGen.Assets.SEWA.Analyser.AudioAnalyser;
using PlayGen.Assets.SEWA.Recorder;
using UnityEngine;

public class MicAnalyserDemo : MonoBehaviour
{
    [Tooltip("Recording Segment Length in Milliseconds")]
    [SerializeField] private int _recordingSegmentLength = 6000;

    private MicAnalyser _micAnalyser;
    private AnalysisResult _currentResult;
    private Stopwatch _lastUpdateTimer;

    private void Awake()
    {
        var monobehaviourContext = this;
        var audioAnalyser = new CommandlineClient(monobehaviourContext);
        _micAnalyser = new MicAnalyser(_recordingSegmentLength, monobehaviourContext, audioAnalyser, OnAnalysisCompleted);
        _lastUpdateTimer = Stopwatch.StartNew();
    }

    private void Start()
    {
        _micAnalyser.StartAnalysis();
    }

    private void OnDestroy()
    {
        _micAnalyser.StopAnalysis();
    }

    // ReSharper disable once InconsistentNaming
    public void OnGUI()
    {
        GUILayout.BeginVertical("box");

        //if (GUILayout.Button("Start"))
        //{
        //    _micAnalyser.StartAnalysis();
        //}
        //else if (GUILayout.Button("End"))
        //{
        //    _micAnalyser.StopAnalysis();
        //}

        GUILayout.Label("Last Update: " + _lastUpdateTimer.ElapsedMilliseconds / 1000f + " seconds.");

        GUILayout.BeginVertical("box");
        
        if (_currentResult != null)
        {
            GUILayout.Label("Analysis Results:");
            foreach (var kvp in _currentResult.Results)
            {
                GUILayout.Label(kvp.Key.PadRight(15, ' ') + "\t " + kvp.Value);
            }
            
            GUILayout.Label("Verbose Analysis Results:");

            GUILayout.Label("Feature:        Classifier, Classifier Type, Classifier Value, ClassifierPrediction Scaled [-1 - 1]");

            foreach (var kvp in _currentResult.VerboseResults)
            {
                GUILayout.Label(kvp.Key.PadRight(15, ' ') + "\t " + kvp.Value.ClassifierPrediction + "\t " + kvp.Value.ClassifierType + ",\t " + kvp.Value.ClassifierValue + ",\t " + kvp.Value.PredictionScaled);
            }
        }

        GUILayout.EndVertical();

        GUILayout.EndVertical();
    }

    public void OnAnalysisCompleted(AnalysisResult result)
    {
        _currentResult = result;
        _lastUpdateTimer.Reset();
        _lastUpdateTimer.Start();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using PlayGen.Assets.SEWA.Analyser;
using PlayGen.Assets.SEWA.Analyser.AudioAnalyser;
using UnityEngine;
using UnityEngine.UI;

public class Scenario : MonoBehaviour
{
    public GameObject Introdiction;
    public Button IntroductionButton;
    public GameObject Brief;
    public Button BreifButton;
    public Lines Lines;
    public GameObject Countdown;
    public GameObject Scene;
    public GameObject AwaitingResults;
    public Results Results;
    public Button ResultsButton;

    public bool IsComplete;

    void OnEnable()
    {
        IsComplete = false;
        StartCoroutine(RunScenario());
    }

    private IEnumerator RunScenario()
    {
        Introdiction.gameObject.SetActive(true);
        Brief.gameObject.SetActive(false);
        Lines.gameObject.SetActive(false);
        Countdown.gameObject.SetActive(false);
        Results.gameObject.SetActive(false);
        AwaitingResults.SetActive(false);
        Scene.SetActive(false);
        Lines.IsDone = false;
        
        IntroductionButton.onClick.RemoveAllListeners();
        var clicked = false;
        IntroductionButton.onClick.AddListener(() => clicked = true);
        while (!clicked)
        {
            yield return null;
        }

        Introdiction.gameObject.SetActive(false);
        Brief.gameObject.SetActive(true);

        BreifButton.onClick.RemoveAllListeners();
        clicked = false;
        BreifButton.onClick.AddListener(() => clicked = true);
        while (!clicked)
        {
            yield return null;
        }

        Brief.gameObject.SetActive(false);
        Scene.SetActive(true);

        Countdown.gameObject.SetActive(true);
        
        var elapsedTime = 0f;
        var totalTime = 3f;
        var countdownDText = Countdown.GetComponentInChildren<Text>();

        while (elapsedTime < totalTime)
        {
            elapsedTime += Time.deltaTime;
            countdownDText.text = Mathf.CeilToInt(totalTime - elapsedTime).ToString();
            yield return null;
        }

        // Start recording Mic
        var frequency = 44100;
        var startTime = Time.time;
        var recording = Microphone.Start("", false, 300, frequency);

        Countdown.gameObject.SetActive(false);
        Lines.gameObject.SetActive(true);

        while (!Lines.IsDone)
        {
            yield return null;
        }

        Scene.SetActive(false);
        Lines.gameObject.SetActive(false);
        AwaitingResults.SetActive(true);

        var recordingLength = Time.time - startTime;
        Microphone.End("");
        var sampleLength = Mathf.CeilToInt(frequency * recordingLength);

        var samples = new float[sampleLength];
        var allSamples = new float[recording.samples];
        recording.GetData(allSamples, 0);
        for (var i = 0; i < sampleLength; i++)
        {
            samples[i] = allSamples[i];
        }

        var audioPath = Application.temporaryCachePath + "/" + Guid.NewGuid() + "temp_recording.wav";
        SavWav.Save(audioPath, samples, recording.frequency, recording.channels);

        var analyzer = new CommandlineClient(this);
        AnalysisResult analysisResult = null;
        analyzer.Analyse(File.ReadAllBytes(audioPath), audioPath, (result) => analysisResult = result);

        while (analysisResult == null)
        {
            yield return null;
        }

        AwaitingResults.SetActive(false);
        Results.SetResults(analysisResult);
        Results.gameObject.SetActive(true);

        ResultsButton.onClick.RemoveAllListeners();
        clicked = false;
        ResultsButton.onClick.AddListener(() => clicked = true);
        while (!clicked)
        {
            yield return null;
        }

        IsComplete = true;
    }
}
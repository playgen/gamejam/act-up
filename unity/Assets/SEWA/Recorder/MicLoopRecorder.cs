﻿using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace PlayGen.Assets.SEWA.Recorder
{
    /// <summary>
    ///  Must be run on the main thread as dictated by the UnityEngine Microphone api.
    /// </summary>
    public class MicLoopRecorder : IDisposable
    {
        private readonly int _recordingLength;
        private readonly MicRecorder _micRecorder;
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private readonly MonoBehaviour _monoBehaviourContext;
        private readonly RecordingCompletedHandler _recordingCompletedHandler;

        private bool _isDisposed;
        private IEnumerator _recordCoroutine;

        public delegate void RecordingCompletedHandler(AudioClip recording);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordingLength">Length of recorded loop segments in Milliseconds</param>
        /// <param name="monoBehaviourContext">Monobehaviour contect to run the Recording Loop on as a Coroutine</param>
        /// <param name="recordingCompletedHandler"></param>
        public MicLoopRecorder(int recordingLength, MonoBehaviour monoBehaviourContext, RecordingCompletedHandler recordingCompletedHandler)
        {
            _recordingLength = recordingLength;
            _monoBehaviourContext = monoBehaviourContext;
            _recordingCompletedHandler = recordingCompletedHandler;
            _micRecorder = new MicRecorder(maxLength: _recordingLength);
        }

        public void Start()
        {
            if (_recordCoroutine != null)
            {
                throw new InvalidOperationException("This instance is already recording. Make sure to stop first or create a new instance.");    
            }

            _recordCoroutine = RecordCoroutine();
            _monoBehaviourContext.StartCoroutine(_recordCoroutine);
        }

        public void Stop()
        {
            if (_recordCoroutine != null)
            {
                _monoBehaviourContext.StopCoroutine(_recordCoroutine);
                _recordCoroutine = null;
            }
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            Stop();

            _isDisposed = true;
        }

        private IEnumerator RecordCoroutine()
        {
            while (true)
            {
                _micRecorder.Stop();
                _micRecorder.Start();
                _stopwatch.Reset();

                if (_micRecorder.HasPrevious)
                {
                    var recording = _micRecorder.GetPrevious();
                    _recordingCompletedHandler(recording);
                }

                // Wait
                var remainingWait = _recordingLength - (int)_stopwatch.ElapsedMilliseconds;
                // If the wait is less than zero, record a new full clip of _recordingLength otherwise cap it to the _recordingLength anyway
                remainingWait = remainingWait < 0
                    ? _recordingLength
                    : remainingWait > _recordingLength
                        ? _recordingLength
                        : remainingWait;
                yield return new WaitForSeconds(remainingWait / 1000f);
            }
        }
    }
}

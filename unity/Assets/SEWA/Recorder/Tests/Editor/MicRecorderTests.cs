﻿using System.Threading;
using NUnit.Framework;

namespace PlayGen.Assets.SEWA.Recorder.Tests.Editor
{
    public class MicRecorderTests
    {
        [Test]
        public void CanRecord()
        {
            // Arrange
            var segmentLength = 1000;
            var micRecorder = new MicRecorder();

            // Act
            micRecorder.Start();
            Thread.Sleep(segmentLength);
            micRecorder.Stop();

            // Assert
            Assert.NotNull(micRecorder.HasPrevious);
            Assert.NotNull(micRecorder.GetPrevious());
        }
    }
}
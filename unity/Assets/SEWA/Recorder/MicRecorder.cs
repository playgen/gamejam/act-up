﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PlayGen.Assets.SEWA.Recorder
{
    /// <summary>
    ///  Must be run on the main thread as dictated by the UnityEngine Microphone api.
    /// </summary>
    public class MicRecorder : IDisposable
    {
        private readonly int _maxLengthSeconds;
        private readonly string _deviceName;
        private readonly int _frequency;
        private AudioClip _currentClip;
        private AudioClip _previousClip;

        private bool _isDisposed;

        public bool HasPrevious
        {
            get { return _previousClip != null; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceName">Default value uses defaulkt recording device</param>
        /// <param name="frequency">Default value uses frequency setting of 44100 Hz</param>
        /// <param name="maxLength">Start length in Milliseconds</param>
        public MicRecorder(string deviceName = "", int frequency = 44100, int maxLength = int.MaxValue)
        {
            _maxLengthSeconds = Mathf.CeilToInt(maxLength / 1000f);
            _deviceName = deviceName;
            _frequency = frequency;
        }

        public void Stop()
        {
            _previousClip = _currentClip;

            if (Microphone.IsRecording(_deviceName))
            {
                Microphone.End(_deviceName);
            }
        }

        public void Start()
        {
            _currentClip = Microphone.Start(_deviceName, false, _maxLengthSeconds, _frequency);
        }

        public AudioClip GetPrevious()
        {
            return _previousClip;
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            Stop();

            _isDisposed = true;
        }
    }
}

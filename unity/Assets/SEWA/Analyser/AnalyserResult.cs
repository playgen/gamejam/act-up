﻿using System.Collections.Generic;
using PlayGen.Assets.SEWA.Analyser.AudioAnalyser;

namespace PlayGen.Assets.SEWA.Analyser
{
    public class AnalysisResult
    {
        public Dictionary<string, float> Results { get; set; }

		public string[] Errors { get; set; }

        public bool HasErrors
        {
            get { return Errors != null && Errors.Length > 0; }
        }

        public Dictionary<string, FeatureResult> VerboseResults { get; set; }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace PlayGen.Assets.SEWA.Analyser.AudioAnalyser
{
    public class WebClient : IAudioAnalyser
    {
        private readonly string _url;
        private readonly MonoBehaviour _monoBehaviourContext;
        private readonly List<IEnumerator> _analyseCoroutines = new List<IEnumerator>();

        private bool _isDisposed;

        public WebClient(string url, MonoBehaviour monoBehaviourContext)
        {
            _url = url;
            _monoBehaviourContext = monoBehaviourContext;
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            _analyseCoroutines.ForEach(_monoBehaviourContext.StopCoroutine);

            _isDisposed = true;
        }

        public void Analyse(byte[] data, string fileName, AnalysisCompletedHandler analysisCompletedHandler)
        {
            var analyseCoroutine = AnalyseCoroutine(data, fileName, analysisCompletedHandler);
            _analyseCoroutines.Add(analyseCoroutine);
            _monoBehaviourContext.StartCoroutine(analyseCoroutine);
        }

        private IEnumerator AnalyseCoroutine(byte[] data, string fileName, AnalysisCompletedHandler analysisCompletedHandler)
        {
            var form = new WWWForm();
            form.AddBinaryData("files", data, fileName);

            var www = new WWW(_url, form);
            yield return www;

            var result = ExtractAnalysisResponse(www);
            analysisCompletedHandler(result);
        }

        private static AnalysisResult ExtractAnalysisResponse(WWW www)
        {
            AnalysisResult result;

            if (!www.isDone)
            {
                result = new AnalysisResult
                {
                    Errors = new[] { "Request Timedout." }
                };
            }
            else if (!string.IsNullOrEmpty(www.error))
            {
                result = new AnalysisResult
                {
                    Errors = new[] { www.error }
                };
            }
            else
            {
                result = JsonConvert.DeserializeObject<AnalysisResult>(www.text);
            }

            return result;
        }
    }
}

﻿
using System;

namespace PlayGen.Assets.SEWA.Analyser.AudioAnalyser
{
    public delegate void AnalysisCompletedHandler(AnalysisResult analysisResult);

    public interface IAudioAnalyser : IDisposable
    {
        void Analyse(byte[] data, string filename, AnalysisCompletedHandler analysisCompletedHandler);
    }
}

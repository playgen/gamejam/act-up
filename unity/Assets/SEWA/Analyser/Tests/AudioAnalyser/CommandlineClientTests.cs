﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using PlayGen.Assets.SEWA.Analyser.AudioAnalyser;
using PlayGen.Assets.SEWA.Tests;
using UnityEngine;
using UnityEngine.TestTools;

namespace PlayGen.Assets.SEWA.Analyser.Tests.AudioAnalyser
{
    public class CommandlineClientTests : PlayModeTests
    {
        [UnityTest]
        public IEnumerator CanAnalyseFile()
        {
            // Arrange
            var audioBytes = File.ReadAllBytes(AudioFilePath);
            var analyser = new CommandlineClient(MonoBehaviourContext);
            AnalysisResult analysisResult = null;

            // Act
            analyser.Analyse(audioBytes, Path.GetFileName(AudioFilePath), result => analysisResult = result);

            while (analysisResult == null)
            {
                yield return new WaitForSeconds(1);
            }

            // Assert
            Assert.NotNull(analysisResult);
            Assert.False(analysisResult.HasErrors);
            Assert.NotNull(analysisResult.Results);
            CollectionAssert.AreEquivalent(CommandlineClient.Features, analysisResult.Results.Keys);
        }
    }
}

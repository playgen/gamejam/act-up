﻿using System;
using System.Collections;
using System.IO;
using NUnit.Framework;
using PlayGen.Assets.SEWA.Analyser.AudioAnalyser;
using PlayGen.Assets.SEWA.Tests;
using UnityEngine;
using UnityEngine.TestTools;

namespace PlayGen.Assets.SEWA.Analyser.Tests.Clients
{
    public class WebClientTests : PlayModeTests
    {
        public const string URL = "http://sewa-image:8080/api/analysis/audio";
        private static readonly string[] ExpectedKeys = { "Arousal", "Liking", "Valence" };

        [UnityTest]
        public IEnumerator CanAnalyseAudio()
        {
            // Arrange
            var client = new WebClient(URL, MonoBehaviourContext);
            var audio = File.ReadAllBytes(AudioFilePath);

            // Act
            AnalysisResult analysisResult = null;
            client.Analyse(audio, Path.GetFileName(AudioFilePath), result => analysisResult = result);

            while (analysisResult == null)
            {
                yield return new WaitForSeconds(1);
            }

            Assert.IsFalse(analysisResult.HasErrors);
            Assert.IsNotNull(analysisResult.Results);
            CollectionAssert.AreEquivalent(ExpectedKeys, analysisResult.Results.Keys);
        }
    }
}

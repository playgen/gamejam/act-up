﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using PlayGen.Assets.SEWA.Analyser.AudioAnalyser;
using PlayGen.Assets.SEWA.Analyser.Tests.Clients;
using PlayGen.Assets.SEWA.Tests;
using UnityEngine;
using UnityEngine.TestTools;

namespace PlayGen.Assets.SEWA.Analyser.Tests
{
    public class MicAnalyserTests : PlayModeTests
    {
        [UnityTest]
        public IEnumerator CanAnalyseMic_WebClient_1_5000()
        {
            return CanAnalyseMic_WebClient(1, 5000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_WebClient_10_5000()
        {
            return CanAnalyseMic_WebClient(10, 5000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_WebClient_1_10000()
        {
            return CanAnalyseMic_WebClient(1, 10000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_WebClient_10_10000()
        {
            return CanAnalyseMic_WebClient(10, 10000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_CommandineClient_1_5000()
        {
            return CanAnalyseMic_CommandlineClient(1, 5000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_CommandineClient_10_5000()
        {
            return CanAnalyseMic_CommandlineClient(10, 5000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_CommandineClient_1_10000()
        {
            return CanAnalyseMic_CommandlineClient(1, 10000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_CommandineClient_10_10000()
        {
            return CanAnalyseMic_CommandlineClient(10, 10000);
        }

        public IEnumerator CanAnalyseMic_WebClient(int itterations, int recordingLength)
        {
            var audioAnalyser = new WebClient(WebClientTests.URL, MonoBehaviourContext);
            return CanAnalyseMic(itterations, recordingLength, audioAnalyser);
        }

        public IEnumerator CanAnalyseMic_CommandlineClient(int itterations, int recordingLength)
        {
            var audioAnalyser = new CommandlineClient(MonoBehaviourContext);
            return CanAnalyseMic(itterations, recordingLength, audioAnalyser);
        }

        public IEnumerator CanAnalyseMic(int itterations, int recordingLength, IAudioAnalyser audioAnalyser)
        {
            // Arrange
            var leeway = 10 * 1000;
            var analysisResults = new List<AnalysisResult>();
            var micAnalyzer = new MicAnalyser(recordingLength, MonoBehaviourContext, audioAnalyser, analysisResults.Add);

            // Act
            micAnalyzer.StartAnalysis();

            yield return new WaitForSeconds((itterations * recordingLength + leeway) / 1000f);

            micAnalyzer.StopAnalysis();

            // Assert
            Assert.GreaterOrEqual(analysisResults.Count, itterations);
            analysisResults.ForEach(Assert.NotNull);
        }
    }
}

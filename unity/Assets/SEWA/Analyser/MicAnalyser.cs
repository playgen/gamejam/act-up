﻿using System;
using System.IO;
using PlayGen.Assets.SEWA.Analyser.AudioAnalyser;
using PlayGen.Assets.SEWA.Recorder;
using UnityEngine;

namespace PlayGen.Assets.SEWA.Analyser
{
    public class MicAnalyser : IDisposable
    {
        private readonly MicLoopRecorder _micLoopRecorder;
        private readonly IAudioAnalyser _audioAnalyser;
        private readonly AnalysisCompletedHandler _analysisCompletedHandler;

        private bool _isDisposed;

        public MicAnalyser(int recordingLength, MonoBehaviour monoBehaviourContext, IAudioAnalyser audioAnalyser, AnalysisCompletedHandler analysisCompletedHandler)
        {
            _micLoopRecorder = new MicLoopRecorder(recordingLength, monoBehaviourContext, OnRecordingCompleted);
            _audioAnalyser = audioAnalyser;
            _analysisCompletedHandler = analysisCompletedHandler;
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            _micLoopRecorder.Dispose();
            _audioAnalyser.Dispose();

            _isDisposed = true;
        }

        public void StartAnalysis()
        {
            _micLoopRecorder.Start();
        }

        public void StopAnalysis()
        {
            _micLoopRecorder.Stop();
        }

        private void OnRecordingCompleted(AudioClip recording)
        {
            var tempPath = Application.temporaryCachePath + "/" + Guid.NewGuid() + "temp_recording.wav";
            if (SavWav.Save(tempPath, recording))
            {
                var audioBytes = File.ReadAllBytes(tempPath);
                _audioAnalyser.Analyse(audioBytes, tempPath, _analysisCompletedHandler);
            }
            else
            {
                Debug.LogWarning("Couldn't get audio bytes for recording.");
            }
        }
    }
}

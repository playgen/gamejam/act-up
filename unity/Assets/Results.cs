﻿using System;
using System.Collections;
using System.Collections.Generic;
using PlayGen.Assets.SEWA.Analyser;
using UnityEngine;
using UnityEngine.UI;

public class Results : MonoBehaviour
{
    [SerializeField] public List<FeatureResultEntry> FeatureResults;

    public void SetResults(AnalysisResult analysisResult)
    {
        DidPass = true;
        foreach (var featureResult in FeatureResults)
        {
            var value = analysisResult.Results[featureResult.Feature];
            DidPass &= value >= featureResult.ExpectedValue;
            featureResult.Slider.value = value;
        }
    }

    public bool DidPass { get; set; }
}

[Serializable]
public class FeatureResultEntry
{
    [SerializeField]
    public Slider Slider;

    [SerializeField]
    public string Feature;

    [SerializeField]
    public float ExpectedValue;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScene : MonoBehaviour
{
    public float Wait;

	// Use this for initialization
	void Start ()
	{
	    StartCoroutine(Coroutine());
	}

    IEnumerator Coroutine()
    {
        yield return new WaitForSeconds(Wait);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}

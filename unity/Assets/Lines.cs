﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lines : MonoBehaviour
{
    public List<LineSettings> LineSettings = new List<LineSettings>();
    private int _currentLine;
    public float PreWait = 1.5f;

    public bool IsDone = false;

    void OnEnable()
    {
        StartCoroutine(ReadLines());
    }

    IEnumerator ReadLines()
    {
        IsDone = false;

        foreach (var lineSetting in LineSettings)
        {
            var image = lineSetting.Line.GetComponentInChildren<Image>();
            image.fillAmount = 0;
        }

        yield return new WaitForSeconds(PreWait);

        foreach (var lineSetting in LineSettings)
        {
            var image = lineSetting.Line.GetComponentInChildren<Image>();
            image.fillAmount = 0;

            while (image.fillAmount != 1)
            {
                image.fillAmount += Time.deltaTime / lineSetting.Duration;
                yield return null;
            }

            var elapsedTime = 0f;
            while (elapsedTime < lineSetting.Pause)
            {
                elapsedTime += Time.deltaTime;
                yield return null;
            }
        }

        IsDone = true;
    }
}

[Serializable]
public class LineSettings
{
    [SerializeField]
    public GameObject Line;

    [SerializeField]
    public float Duration;

    [SerializeField]
    public float Pause;
}
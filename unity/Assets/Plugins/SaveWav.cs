﻿//	Copyright (c) 2012 Calvin Rien
//        http://the.darktable.com
//
//	This software is provided 'as-is', without any express or implied warranty. In
//	no event will the authors be held liable for any damages arising from the use
//	of this software.
//
//	Permission is granted to anyone to use this software for any purpose,
//	including commercial applications, and to alter it and redistribute it freely,
//	subject to the following restrictions:
//
//	1. The origin of this software must not be misrepresented; you must not claim
//	that you wrote the original software. If you use this software in a product,
//	an acknowledgment in the product documentation would be appreciated but is not
//	required.
//
//	2. Altered source versions must be plainly marked as such, and must not be
//	misrepresented as being the original software.
//
//	3. This notice may not be removed or altered from any source distribution.
//
//  =============================================================================
//
//  derived from Gregorio Zanon's script
//  http://forum.unity3d.com/threads/119295-Writing-AudioListener.GetOutputData-to-wav-problem?p=806734&viewfull=1#post806734

using System;
using System.IO;
using UnityEngine;
using System.Collections.Generic;

public static class SavWav
{

    const int HEADER_SIZE = 44;

    public static bool Save(string filepath, AudioClip clip)
    {
        // Make sure directory exists if user is saving to sub dir.
        Directory.CreateDirectory(Path.GetDirectoryName(filepath));

        using (var fileStream = CreateEmpty(filepath))
        {

            ConvertAndWrite(fileStream, clip);

            WriteHeader(fileStream, clip);
        }

        return true; // TODO: return false if there's a failure saving the file
    }

    public static bool Save(string filepath, float[] samples, int hz, int channels)
    {
        // Make sure directory exists if user is saving to sub dir.
        Directory.CreateDirectory(Path.GetDirectoryName(filepath));

        using (var fileStream = CreateEmpty(filepath))
        {

            ConvertAndWrite(fileStream, samples);

            WriteHeader(fileStream, hz, channels, samples.Length);
        }

        return true; // TODO: return false if there's a failure saving the file
    }

    public static AudioClip TrimSilence(AudioClip clip, float min)
    {
        var samples = new float[clip.samples];

        clip.GetData(samples, 0);

        return TrimSilence(new List<float>(samples), min, clip.channels, clip.frequency);
    }

    public static AudioClip TrimSilence(List<float> samples, float min, int channels, int hz)
    {
        return TrimSilence(samples, min, channels, hz, false, false);
    }

    public static AudioClip TrimSilence(List<float> samples, float min, int channels, int hz, bool _3D, bool stream)
    {
        int i;

        for (i = 0; i < samples.Count; i++)
        {
            if (Mathf.Abs(samples[i]) > min)
            {
                break;
            }
        }

        samples.RemoveRange(0, i);

        for (i = samples.Count - 1; i > 0; i--)
        {
            if (Mathf.Abs(samples[i]) > min)
            {
                break;
            }
        }

        samples.RemoveRange(i, samples.Count - i);

        var clip = AudioClip.Create("TempClip", samples.Count, channels, hz, _3D, stream);

        clip.SetData(samples.ToArray(), 0);

        return clip;
    }

    static FileStream CreateEmpty(string filepath)
    {
        var fileStream = new FileStream(filepath, FileMode.Create);
        byte emptyByte = new byte();

        for (int i = 0; i < HEADER_SIZE; i++) //preparing the header
        {
            fileStream.WriteByte(emptyByte);
        }

        return fileStream;
    }

    static void ConvertAndWrite(FileStream fileStream, AudioClip clip)
    {

        var samples = new float[clip.samples];

        clip.GetData(samples, 0);

       ConvertAndWrite(fileStream, samples);
    }

    static void ConvertAndWrite(FileStream fileStream, float[] samples)
    {

        Int16[] intData = new Int16[samples.Length];
        //converting in 2 float[] steps to Int16[], //then Int16[] to Byte[]

        Byte[] bytesData = new Byte[samples.Length * 2];
        //bytesData array is twice the size of
        //dataSource array because a float converted in Int16 is 2 bytes.

        int rescaleFactor = 32767; //to convert float to Int16

        for (int i = 0; i < samples.Length; i++)
        {
            intData[i] = (short)(samples[i] * rescaleFactor);
            Byte[] byteArr = new Byte[2];
            byteArr = BitConverter.GetBytes(intData[i]);
            byteArr.CopyTo(bytesData, i * 2);
        }

        fileStream.Write(bytesData, 0, bytesData.Length);
    }



    static void WriteHeader(FileStream fileStream, AudioClip clip)
    {
        var hz = clip.frequency;
        var channels = clip.channels;
        var samples = clip.samples;

        WriteHeader(fileStream, hz, channels, samples);
    }

    static void WriteHeader(FileStream fileStream, int hz, int channels, int samples)
    {

        fileStream.Seek(0, SeekOrigin.Begin);

        Byte[] riff = System.Text.Encoding.UTF8.GetBytes("RIFF");
        fileStream.Write(riff, 0, 4);

        Byte[] chunkSize = BitConverter.GetBytes(fileStream.Length - 8);
        fileStream.Write(chunkSize, 0, 4);

        Byte[] wave = System.Text.Encoding.UTF8.GetBytes("WAVE");
        fileStream.Write(wave, 0, 4);

        Byte[] fmt = System.Text.Encoding.UTF8.GetBytes("fmt ");
        fileStream.Write(fmt, 0, 4);

        Byte[] subChunk1 = BitConverter.GetBytes(16);
        fileStream.Write(subChunk1, 0, 4);

        UInt16 two = 2;
        UInt16 one = 1;

        Byte[] audioFormat = BitConverter.GetBytes(one);
        fileStream.Write(audioFormat, 0, 2);

        Byte[] numChannels = BitConverter.GetBytes(channels);
        fileStream.Write(numChannels, 0, 2);

        Byte[] sampleRate = BitConverter.GetBytes(hz);
        fileStream.Write(sampleRate, 0, 4);

        Byte[] byteRate = BitConverter.GetBytes(hz * channels * 2); // sampleRate * bytesPerSample*number of channels, here 44100*2*2
        fileStream.Write(byteRate, 0, 4);

        UInt16 blockAlign = (ushort)(channels * 2);
        fileStream.Write(BitConverter.GetBytes(blockAlign), 0, 2);

        UInt16 bps = 16;
        Byte[] bitsPerSample = BitConverter.GetBytes(bps);
        fileStream.Write(bitsPerSample, 0, 2);

        Byte[] datastring = System.Text.Encoding.UTF8.GetBytes("data");
        fileStream.Write(datastring, 0, 4);

        Byte[] subChunk2 = BitConverter.GetBytes(samples * channels * 2);
        fileStream.Write(subChunk2, 0, 4);

        //		fileStream.Close();
    }
}
/*

#pragma strict

import System.IO; // for FileStream
import System; // for BitConverter and Byte Type

private var pintBufferSize : int;
private var pintNumBuffers : int;
private var pintOutputRate : int = 44100;
private var pstrFilename : String = "recTest.wav";
private var pintHeaderSize : int = 44; //default for uncompressed wav
 
private var pbooOutput : boolean;
 
private var fileStream : FileStream;
 
// Audio Source
private var pausAudio : AudioSource;
 
function Awake()
{

AudioSettings.outputSampleRate = pintOutputRate;
}

function Start()
{

AudioSettings.GetDSPBufferSize(pintBufferSize, pintNumBuffers);

// Make a reference to the attached audio source
pausAudio = gameObject.GetComponent(AudioSource);
}

function Update()
{

if (s.clpMicrophone != null)
{
// Copy over the clip
pausAudio.clip = s.clpMicrophone;
// Clear the global clip
s.clpMicrophone = null;
// Is the clip useful?
if (pausAudio.clip != null)
{
    // Play the clip
    pausAudio.Play();
    // Transcribe the audio clip to become a .WAV file
    StartWriting(pstrFilename);
    // Flag for subsequent Update() visits
    pbooOutput = true;
}
}

// Test the flag to see if an audio clip is being transcribed to be a .WAV file
if (pbooOutput == true)
{
// Is the clip done?
if (!pausAudio.isPlaying)
{
// The output is done
pbooOutput = false;
// Write the .WAV file header
WriteHeader();
}
}
}

function StartWriting(name : String)
{

fileStream = new FileStream(name, FileMode.Create);
var emptyByte : byte = new byte();

for (var i : int = 0; i < pintHeaderSize; i++){
//preparing the header
fileStream.WriteByte(emptyByte);
}
}

function OnAudioFilterRead(data : float[], channels : int)
{
if (pbooOutput)
{
ConvertAndWrite(data);      //audio data is interlaced
}
}

function ConvertAndWrite(dataSource : float[])
{

var intData : Int16[] = new Int16[dataSource.length];
//converting in 2 steps : float[] to Int16[], //then Int16[] to Byte[]

var bytesData : Byte[] = new Byte[dataSource.length * 2];
//bytesData array is twice the size of
//dataSource array because a float converted in Int16 is 2 bytes.

var rescaleFactor : int = 32767; //to convert float to Int16

for (var i : int = 0; i < dataSource.length; i++){
intData[i] = dataSource[i] * rescaleFactor;
var byteArr : Byte[] = new Byte[2];
byteArr = BitConverter.GetBytes(intData[i]);
byteArr.CopyTo(bytesData, i * 2);
}

fileStream.Write(bytesData, 0, bytesData.length);
}

function WriteHeader()
{

fileStream.Seek(0, SeekOrigin.Begin);

var riff : Byte[] = System.Text.Encoding.UTF8.GetBytes("RIFF");
fileStream.Write(riff, 0, 4);

var chunkSize : Byte[] = BitConverter.GetBytes(fileStream.Length - 8);
fileStream.Write(chunkSize, 0, 4);

var wave : Byte[] = System.Text.Encoding.UTF8.GetBytes("WAVE");
fileStream.Write(wave, 0, 4);

var fmt : Byte[] = System.Text.Encoding.UTF8.GetBytes("fmt ");
fileStream.Write(fmt, 0, 4);

var subChunk1 : Byte[] = BitConverter.GetBytes(16);
fileStream.Write(subChunk1, 0, 4);

var two : UInt16 = 2;
var one : UInt16 = 1;

var audioFormat : Byte[] = BitConverter.GetBytes(one);
fileStream.Write(audioFormat, 0, 2);

var numChannels : Byte[] = BitConverter.GetBytes(two);
fileStream.Write(numChannels, 0, 2);

var sampleRate : Byte[] = BitConverter.GetBytes(pintOutputRate);
fileStream.Write(sampleRate, 0, 4);

var byteRate : Byte[] = BitConverter.GetBytes(pintOutputRate * 4);
// sampleRate * bytesPerSample*number of channels, here 44100*2*2

fileStream.Write(byteRate, 0, 4);

var four : UInt16 = 4;
var blockAlign : Byte[] = BitConverter.GetBytes(four);
fileStream.Write(blockAlign, 0, 2);

var sixteen : UInt16 = 16;
var bitsPerSample : Byte[] = BitConverter.GetBytes(sixteen);
fileStream.Write(bitsPerSample, 0, 2);

var dataString : Byte[] = System.Text.Encoding.UTF8.GetBytes("data");
fileStream.Write(dataString, 0, 4);

var subChunk2 : Byte[] = BitConverter.GetBytes(fileStream.Length - pintHeaderSize);
fileStream.Write(subChunk2, 0, 4);

fileStream.Close();
}*/

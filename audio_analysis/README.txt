Demonstrator: Prediction of several high-level features from audio with openXBOW

List of features:
-Anger
-Boredom
-Conflict
-Deception
-Happiness
-Interest
-Likability
-Positivity
-Sadness
-Sincerity
-Sleepiness
-Stress
-Volume

For each feature, a separate file in JSON format ("$Feature.json") is written and printed on stdout.

Output (JSON):
{
    "Index": "0",              -> Index of the analysis window. It is always 0 for segment-level prediction and it is the index of the window in time-continuous analysis (timestamp = index x hopsize).
    "Feature": "FeatureName",  -> Name of the features.
    "Class1prob": 0.7445425,   -> Probability of class 1 (class 1 = contrary of the corresponding feature detected).
    "Class2prob": 0.06307668,  -> Probability of class 2 (class 2 = neither feature nor its contrary detected).
    "Class3prob": 0.19238085,  -> Probability of class 3 (class 3 = corresponding feature detected). Probability (0 <= prob <= 1) can be used to estimate the magnitude of the corresponding feature.
    "Prediction": 1,           -> Predicted class (most likely of the 3 classes).
}

The features Anger, Conflict, Deception, Likability, & Sadness are output in two classes, where class 1 means that feature is not detected and class 2 means that the corresponding feature is detected.


To run the analysis/prediction, use the bash scripts:

./analyse.sh filename.wav
with pcm wavefile (16-bit) filename.wav, to predict the features from the wavefile 'filename.wav' (should have a duration of 4s-10s)
OR
./record_and_analyse.sh
to record audio with your default microphone and predict from the recording. record_and_analyse.sh requires the package sox.

Under Linux, remove ".exe" in line 11 of analyse.sh

To enable time-continuous prediction, uncomment lines 9 and 18 in analyse.sh and outcomment line 17.

Contact: {maximilian.schmitt,bjoern.schuller}@uni-passau.de
(c) University of Passau, June 2017

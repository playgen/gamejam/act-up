#!/bin/bash
# Record audio file and predict high-level features using bag-of-audio-words
# April2017, Chair of Complex and Intelligent Systems, University of Passau
# SEWA project

# Requires the toolkit sox

rec -b 16 -r 16000 recording.wav silence 1 0.1 3% 1 1.0 3%

./analyse.sh recording.wav

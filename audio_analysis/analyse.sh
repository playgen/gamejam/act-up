#!/bin/bash
# Prediction of high-level features using bag-of-audio-words
# April2017, Chair of Complex and Intelligent Systems, University of Passau
# SEWA project

wavfile=$1  # Input in wave format (PCM, 16 bit, arbitrary sampling rate, mono or stereo)
test -z "$wavfile" && wavfile="input.wav"

#hopsize=1.0

./SMILExtract.exe -C "SMILE.ComParE.LLD.conf" -logfile smile.log -I $wavfile -instname $wavfile -lldcsvoutput "LLD.csv" -l 1  # Extract LLDs with openSMILE (not part of SEWA, no commericial use!)

declare -a featureNames=("Anger" "Boredom" "Conflict" "Deception" "Happiness" "Interest" "Likability" "Positivity" "Sadness" "Sincerity" "Sleepiness" "Stress" "Volume")

for featureName in "${featureNames[@]}"
do
   java -jar openXBOW.jar -i LLD.csv -svmModel models/$featureName.model -attributes nt1[65]2[65] -norm 1 -b models/$featureName.codebook &>/dev/null
   #java -jar openXBOW.jar -i LLD.csv -svmModel models/$featureName.model -attributes nt1[65]2[65] -norm 1 -b models/$featureName.codebook -t 6.0 $hopsize &>/dev/null  # USE THIS IN CASE TIME-CONTINUOUS PREDICTIONS ARE NEEDED
   cat $featureName.json
done
